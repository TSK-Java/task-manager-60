package ru.tsc.kirillov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.ApplicationAboutRequest;
import ru.tsc.kirillov.tm.dto.response.ApplicationAboutResponse;
import ru.tsc.kirillov.tm.event.ConsoleEvent;

@Component
public final class ApplicationAboutListener extends AbstractSystemListener {

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-a";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отображение информации о разработчике.";
    }

    @Override
    @EventListener(condition = "@applicationAboutListener.getName() == #event.name || @applicationAboutListener.getArgument() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("\n[О клиенте]");
        System.out.println("Разработчик: " + getPropertyService().getAuthorName());
        System.out.println("E-mail: " + getPropertyService().getAuthorEmail());

        System.out.println("\n[О сервере]");
        ApplicationAboutResponse response = getSystemEndpoint().getAbout(new ApplicationAboutRequest());
        System.out.println("Разработчик: " + response.getName());
        System.out.println("E-mail: " + response.getEmail());
    }

}

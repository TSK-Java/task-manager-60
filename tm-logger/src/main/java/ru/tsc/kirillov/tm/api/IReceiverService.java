package ru.tsc.kirillov.tm.api;

import org.jetbrains.annotations.NotNull;

import javax.jms.MessageListener;

public interface IReceiverService {

    void init(@NotNull MessageListener listener);
    
}

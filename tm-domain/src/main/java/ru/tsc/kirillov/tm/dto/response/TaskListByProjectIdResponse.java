package ru.tsc.kirillov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.TaskDto;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TaskListByProjectIdResponse extends AbstractResponse {

    @Nullable
    private List<TaskDto> tasks;

    public TaskListByProjectIdResponse(@Nullable final List<TaskDto> tasks) {
        this.tasks = tasks;
    }

}

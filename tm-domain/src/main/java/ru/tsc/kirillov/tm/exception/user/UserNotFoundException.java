package ru.tsc.kirillov.tm.exception.user;

public final class UserNotFoundException extends AbstractUserException {

    public UserNotFoundException() {
        super("Ошибка! Пользователь не найден.");
    }

}

package ru.tsc.kirillov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

public enum FileEventKind {

    CREATE("Создан"),
    MODIFY("Отредактирован"),
    DELETE("Удалён"),
    OVERFLOW("Не обработан");

    @Getter
    @NotNull
    private final String displayName;

    FileEventKind(@NotNull final String displayName) {
        this.displayName = displayName;
    }

}

package ru.tsc.kirillov.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.kirillov.tm.api.repository.dto.IProjectDtoRepository;
import ru.tsc.kirillov.tm.dto.model.AbstractUserOwnedDtoModel_;
import ru.tsc.kirillov.tm.dto.model.ProjectDto;
import ru.tsc.kirillov.tm.dto.model.ProjectDto_;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
@NoArgsConstructor
public class ProjectDtoRepository
        extends AbstractWbsDtoRepository<ProjectDto>
        implements IProjectDtoRepository {

    @NotNull
    protected CriteriaQuery<String> selectAllIdByUserId(@Nullable final String userId) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<String> criteriaQuery = criteriaBuilder.createQuery(String.class);
        @NotNull final Root<ProjectDto> root = criteriaQuery.from(modelClass);
        return criteriaQuery
                .select(root.get(ProjectDto_.id))
                .where(
                        criteriaBuilder.equal(root.get(AbstractUserOwnedDtoModel_.userId), userId)
                );
    }

    @NotNull
    @Override
    public String[] findAllId(@Nullable final String userId) {
        if (userId == null) return new String[]{};
        @NotNull final List<String> result = entityManager
                .createQuery(selectAllIdByUserId(userId))
                .getResultList();
        return result.toArray(new String[]{});
    }

}

package ru.tsc.kirillov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.model.AbstractWbsModel;
import ru.tsc.kirillov.tm.model.User;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface IWbsRepository<M extends AbstractWbsModel> extends IUserOwnedRepository<M> {

    @Nullable
    M create(@Nullable User user, @NotNull String name);

    @Nullable
    M create(@Nullable User user, @NotNull String name, @NotNull String description);

    @Nullable
    M create(
            @Nullable User user,
            @NotNull String name,
            @NotNull String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    );

    @NotNull
    List<M> findAll(final Comparator<M> comparator);

    @NotNull
    List<M> findAll(@Nullable String userId, @NotNull Comparator<M> comparator);

}

package ru.tsc.kirillov.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.kirillov.tm.api.repository.model.IUserRepository;
import ru.tsc.kirillov.tm.model.User;
import ru.tsc.kirillov.tm.model.User_;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Optional;

@Repository
@NoArgsConstructor
public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    protected CriteriaQuery<User> selectUserByLogin(@Nullable final String login) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(modelClass);
        @NotNull final Root<User> root = criteriaQuery.from(modelClass);
        return criteriaQuery
                .select(root)
                .where(
                        criteriaBuilder.equal(root.get(User_.login), login)
                );
    }

    @NotNull
    protected CriteriaQuery<User> selectUserByEmail(@Nullable final String email) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(modelClass);
        @NotNull final Root<User> root = criteriaQuery.from(modelClass);
        return criteriaQuery
                .select(root)
                .where(
                        criteriaBuilder.equal(root.get(User_.email), email)
                );
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        @NotNull final TypedQuery<User> query = entityManager
                .createQuery(selectUserByLogin(login))
                .setHint("org.hibernate.cacheable", true);
        return getResult(query);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        @NotNull final TypedQuery<User> query = entityManager
                .createQuery(selectUserByEmail(email))
                .setHint("org.hibernate.cacheable", true);
        return getResult(query);
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        @NotNull final Optional<User> user = Optional.ofNullable(findByLogin(login));
        user.ifPresent(this::remove);
        return user.orElse(null);
    }

}

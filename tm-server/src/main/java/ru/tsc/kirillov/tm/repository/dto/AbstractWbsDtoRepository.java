package ru.tsc.kirillov.tm.repository.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.kirillov.tm.api.repository.dto.IWbsDtoRepository;
import ru.tsc.kirillov.tm.comparator.CreatedComparator;
import ru.tsc.kirillov.tm.comparator.DateBeginComparator;
import ru.tsc.kirillov.tm.comparator.StatusComparator;
import ru.tsc.kirillov.tm.dto.model.AbstractWbsDtoModel;
import ru.tsc.kirillov.tm.dto.model.AbstractWbsDtoModel_;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Repository
public abstract class AbstractWbsDtoRepository<M extends AbstractWbsDtoModel>
        extends AbstractUserOwnedDtoRepository<M>
        implements IWbsDtoRepository<M> {

    @NotNull
    protected SingularAttribute<AbstractWbsDtoModel, ?> getColumnSort(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return AbstractWbsDtoModel_.created;
        else if (comparator == StatusComparator.INSTANCE) return AbstractWbsDtoModel_.status;
        else if (comparator == DateBeginComparator.INSTANCE) return AbstractWbsDtoModel_.dateBegin;
        else return AbstractWbsDtoModel_.name;
    }

    @NotNull
    protected CriteriaQuery<M> selectModelOrder(@NotNull final Comparator<M> comparator) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<M> criteriaQuery = criteriaBuilder.createQuery(modelClass);
        @NotNull final Root<M> root = criteriaQuery.from(modelClass);
        return criteriaQuery
                .select(root)
                .orderBy(
                        criteriaBuilder.asc(root.get(getColumnSort(comparator)))
                );
    }

    @NotNull
    protected CriteriaQuery<M> selectModelByUserIdOrder(
            @Nullable final String userId,
            @NotNull final Comparator<M> comparator
    ) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<M> criteriaQuery = criteriaBuilder.createQuery(modelClass);
        @NotNull final Root<M> root = criteriaQuery.from(modelClass);
        return criteriaQuery
                .select(root)
                .where(
                        criteriaBuilder.equal(root.get(AbstractWbsDtoModel_.userId), userId)
                )
                .orderBy(
                        criteriaBuilder.asc(root.get(getColumnSort(comparator)))
                );
    }

    @Nullable
    @Override
    @SneakyThrows
    public M create(@Nullable final String userId, @NotNull final String name) {
        @NotNull final M model = newInstance();
        model.setName(name);
        model.setUserId(userId);
        return add(model);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M create(@Nullable final String userId, @NotNull final String name, @NotNull final String description) {
        @NotNull final M model = newInstance();
        model.setName(name);
        model.setUserId(userId);
        model.setDescription(description);
        return add(model);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M create(
            @Nullable final String userId,
            @NotNull final String name,
            @NotNull final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        @NotNull final M model = newInstance();
        model.setName(name);
        model.setUserId(userId);
        model.setDescription(description);
        model.setDateBegin(dateBegin);
        model.setDateEnd(dateEnd);
        return add(model);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @NotNull final Comparator<M> comparator) {
        if (userId == null) return Collections.emptyList();
        return entityManager
                .createQuery(selectModelByUserIdOrder(userId, comparator))
                .getResultList();
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        return entityManager
                .createQuery(selectModelOrder(comparator))
                .getResultList();
    }

}

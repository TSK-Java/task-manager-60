package ru.tsc.kirillov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.TaskDto;

import java.util.List;

public interface ITaskDtoRepository extends IWbsDtoRepository<TaskDto> {

    @NotNull
    List<TaskDto> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void removeAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void removeAllByProjectList(@Nullable String userId, @Nullable String[] projects);

}

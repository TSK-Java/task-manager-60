package ru.tsc.kirillov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.ProjectDto;

public interface IProjectDtoService extends IWbsDtoService<ProjectDto> {

    @NotNull
    String[] findAllId(@Nullable final String userId);

}

package ru.tsc.kirillov.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.kirillov.tm.api.repository.model.ITaskRepository;
import ru.tsc.kirillov.tm.model.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
@NoArgsConstructor
public class TaskRepository extends AbstractWbsRepository<Task> implements ITaskRepository {

    @NotNull
    protected CriteriaQuery<Task> selectTaskProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Task> criteriaQuery = criteriaBuilder.createQuery(modelClass);
        @NotNull final Root<Task> root = criteriaQuery.from(modelClass);
        @NotNull final Join<Task, User> taskUser = root.join(AbstractUserOwnedModel_.user);
        @NotNull final Join<Task, Project> taskProject = root.join(Task_.project);
        return criteriaQuery
                .select(root)
                .where(
                        criteriaBuilder.equal(taskUser.get(User_.id), userId),
                        criteriaBuilder.equal(taskProject.get(Project_.id), projectId)
                );
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        return entityManager
                .createQuery(selectTaskProjectId(userId, projectId))
                .getResultList();
    }

    @Override
    public void removeAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        removeAll(findAllByProjectId(userId, projectId));
    }

}

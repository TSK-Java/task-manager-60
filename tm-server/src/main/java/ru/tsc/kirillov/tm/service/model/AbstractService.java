package ru.tsc.kirillov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kirillov.tm.api.repository.model.IRepository;
import ru.tsc.kirillov.tm.api.service.model.IService;
import ru.tsc.kirillov.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kirillov.tm.exception.field.IdEmptyException;
import ru.tsc.kirillov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.List;

@Service
public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {
    
    @NotNull
    protected abstract IRepository<M> getRepository();

    @Nullable
    @Override
    @Transactional
    public M add(@Nullable final M model) {
        if (model == null) return null;
        @NotNull final IRepository<M> repository = getRepository();
        return repository.add(model);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> add(@NotNull final Collection<M> models) {
        @NotNull final IRepository<M> repository = getRepository();
        return repository.add(models);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> set(@NotNull final Collection<M> models) {
        @NotNull final IRepository<M> repository = getRepository();
        return repository.set(models);
    }

    @Override
    @Transactional
    public void clear() {
        @NotNull final IRepository<M> repository = getRepository();
        repository.clear();
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull final IRepository<M> repository = getRepository();
        return repository.findAll();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        @NotNull final IRepository<M> repository = getRepository();
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IRepository<M> repository = getRepository();
        return repository.findOneById(id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexOutOfBoundsException();
        @NotNull final IRepository<M> repository = getRepository();
        return repository.findOneByIndex(index);
    }

    @Nullable
    @Override
    @Transactional
    public M remove(@Nullable final M model) {
        if (model == null) return null;
        @NotNull final IRepository<M> repository = getRepository();
        return repository.remove(model);
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        @NotNull final IRepository<M> repository = getRepository();
        repository.removeAll(collection);
    }

    @Nullable
    @Override
    @Transactional
    public M removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IRepository<M> repository = getRepository();
        @Nullable final M result;
        result = repository.removeById(id);
        if (result == null) throw new EntityNotFoundException();
        return result;
    }

    @Nullable
    @Override
    @Transactional
    public M removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexOutOfBoundsException();
        @NotNull final IRepository<M> repository = getRepository();
        return repository.removeByIndex(index);
    }

    @Nullable
    @Override
    @Transactional
    public M update(@Nullable final M model) {
        if (model == null) return null;
        @NotNull final IRepository<M> repository = getRepository();
        return repository.update(model);
    }

    @Override
    public long count() {
        @NotNull final IRepository<M> repository = getRepository();
        return repository.count();
    }

}

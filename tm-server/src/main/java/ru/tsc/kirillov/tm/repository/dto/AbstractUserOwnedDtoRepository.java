package ru.tsc.kirillov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.kirillov.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.tsc.kirillov.tm.dto.model.AbstractUserOwnedDtoModel;
import ru.tsc.kirillov.tm.dto.model.AbstractUserOwnedDtoModel_;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

@Repository
public abstract class AbstractUserOwnedDtoRepository<M extends AbstractUserOwnedDtoModel>
        extends AbstractDtoRepository<M>
        implements IUserOwnedDtoRepository<M> {

    @NotNull
    protected CriteriaQuery<M> selectModelByUserId(@Nullable final String userId) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<M> criteriaQuery = criteriaBuilder.createQuery(modelClass);
        @NotNull final Root<M> root = criteriaQuery.from(modelClass);
        return criteriaQuery
                .select(root)
                .where(
                        criteriaBuilder.equal(root.get(AbstractUserOwnedDtoModel_.userId), userId)
                );
    }

    @NotNull
    protected CriteriaQuery<M> selectModelByUserIdAndId(@Nullable final String userId, @NotNull final String id) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<M> criteriaQuery = criteriaBuilder.createQuery(modelClass);
        @NotNull final Root<M> root = criteriaQuery.from(modelClass);
        return criteriaQuery
                .select(root)
                .where(
                        criteriaBuilder.equal(root.get(AbstractUserOwnedDtoModel_.userId), userId),
                        criteriaBuilder.equal(root.get(AbstractUserOwnedDtoModel_.id), id)
                );
    }

    @NotNull
    protected CriteriaQuery<Long> selectCountModelByUserId(@Nullable final String userId) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        @NotNull final Root<M> root = criteriaQuery.from(modelClass);
        return criteriaQuery
                .select(criteriaBuilder.count(root))
                .where(
                        criteriaBuilder.equal(root.get(AbstractUserOwnedDtoModel_.userId), userId)
                );
    }

    @NotNull
    protected CriteriaQuery<Long> selectCountModelByUserIdAndId(
            @Nullable final String userId,
            @NotNull final String id
    ) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        @NotNull final Root<M> root = criteriaQuery.from(modelClass);
        return criteriaQuery
                .select(criteriaBuilder.count(root))
                .where(
                        criteriaBuilder.equal(root.get(AbstractUserOwnedDtoModel_.userId), userId),
                        criteriaBuilder.equal(root.get(AbstractUserOwnedDtoModel_.id), id)
                );
    }

    @NotNull
    protected CriteriaDelete<M> deleteModelByUserId(@Nullable final String userId) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaDelete<M> criteriaDelete = criteriaBuilder.createCriteriaDelete(modelClass);
        @NotNull final Root<M> root = criteriaDelete.from(modelClass);
        return criteriaDelete
                .where(
                        criteriaBuilder.equal(root.get(AbstractUserOwnedDtoModel_.userId), userId)
                );
    }

    @Override
    public void clear(@Nullable final String userId) {
        entityManager
                .createQuery(deleteModelByUserId(userId))
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        return entityManager
                .createQuery(selectModelByUserId(userId))
                .getResultList();
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return false;
        return entityManager
                .createQuery(selectCountModelByUserIdAndId(userId, id))
                .getSingleResult() == 1L;
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @NotNull final TypedQuery<M> query = entityManager
                .createQuery(selectModelByUserIdAndId(userId, id));
        return getResult(query);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || index == null) return null;
        @NotNull final TypedQuery<M> query = entityManager
                .createQuery(selectModelByUserId(userId))
                .setFirstResult(index);
        return getResult(query);
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @NotNull final Optional<M> model = Optional.ofNullable(findOneById(userId, id));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || index == null) return null;
        @NotNull final Optional<M> model = Optional.ofNullable(findOneByIndex(userId, index));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }

    @Override
    public long count(@Nullable final String userId) {
        if (userId == null) return 0;
        return entityManager
                .createQuery(selectCountModelByUserId(userId))
                .getSingleResult();
    }

}

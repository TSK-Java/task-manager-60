package ru.tsc.kirillov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.UserDto;

public interface IUserDtoRepository extends IDtoRepository<UserDto> {

    @Nullable
    UserDto findByLogin(@NotNull String login);

    @Nullable
    UserDto findByEmail(@NotNull String email);

    @Nullable
    UserDto removeByLogin(@NotNull String login);

}

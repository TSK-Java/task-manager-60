package ru.tsc.kirillov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.AbstractWbsDtoModel;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface IWbsDtoRepository<M extends AbstractWbsDtoModel> extends IUserOwnedDtoRepository<M> {

    @Nullable
    M create(@Nullable String userId, @NotNull String name);

    @Nullable
    M create(@Nullable String userId, @NotNull String name, @NotNull String description);

    @Nullable
    M create(
            @Nullable String userId,
            @NotNull String name,
            @NotNull String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    );

    @NotNull
    List<M> findAll(final Comparator<M> comparator);

    @NotNull
    List<M> findAll(@Nullable String userId, @NotNull Comparator<M> comparator);

}

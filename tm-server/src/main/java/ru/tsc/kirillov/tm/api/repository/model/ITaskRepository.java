package ru.tsc.kirillov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IWbsRepository<Task> {

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable final String projectId);

    void removeAllByProjectId(@Nullable String userId, @Nullable String projectId);

}

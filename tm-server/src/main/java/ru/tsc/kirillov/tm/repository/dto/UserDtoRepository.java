package ru.tsc.kirillov.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.kirillov.tm.api.repository.dto.IUserDtoRepository;
import ru.tsc.kirillov.tm.dto.model.UserDto;
import ru.tsc.kirillov.tm.dto.model.UserDto_;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Optional;

@Repository
@NoArgsConstructor
public class UserDtoRepository extends AbstractDtoRepository<UserDto> implements IUserDtoRepository {

    @NotNull
    protected CriteriaQuery<UserDto> selectUserByLogin(@Nullable final String login) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<UserDto> criteriaQuery = criteriaBuilder.createQuery(modelClass);
        @NotNull final Root<UserDto> root = criteriaQuery.from(modelClass);
        return criteriaQuery
                .select(root)
                .where(
                        criteriaBuilder.equal(root.get(UserDto_.login), login)
                );
    }

    @NotNull
    protected CriteriaQuery<UserDto> selectUserByEmail(@Nullable final String email) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<UserDto> criteriaQuery = criteriaBuilder.createQuery(modelClass);
        @NotNull final Root<UserDto> root = criteriaQuery.from(modelClass);
        return criteriaQuery
                .select(root)
                .where(
                        criteriaBuilder.equal(root.get(UserDto_.email), email)
                );
    }
    
    @Nullable
    @Override
    public UserDto findByLogin(@NotNull final String login) {
        @NotNull final TypedQuery<UserDto> query = entityManager
                .createQuery(selectUserByLogin(login))
                .setHint("org.hibernate.cacheable", true);
        return getResult(query);
    }

    @Nullable
    @Override
    public UserDto findByEmail(@NotNull final String email) {
        @NotNull final TypedQuery<UserDto> query = entityManager
                .createQuery(selectUserByEmail(email))
                .setHint("org.hibernate.cacheable", true);
        return getResult(query);
    }

    @Nullable
    @Override
    public UserDto removeByLogin(@NotNull final String login) {
        @NotNull final Optional<UserDto> user = Optional.ofNullable(findByLogin(login));
        user.ifPresent(this::remove);
        return user.orElse(null);
    }

}

package ru.tsc.kirillov.tm.repository.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.kirillov.tm.api.repository.model.IWbsRepository;
import ru.tsc.kirillov.tm.comparator.CreatedComparator;
import ru.tsc.kirillov.tm.comparator.DateBeginComparator;
import ru.tsc.kirillov.tm.comparator.StatusComparator;
import ru.tsc.kirillov.tm.model.AbstractWbsModel;
import ru.tsc.kirillov.tm.model.AbstractWbsModel_;
import ru.tsc.kirillov.tm.model.User;
import ru.tsc.kirillov.tm.model.User_;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Repository
public abstract class AbstractWbsRepository<M extends AbstractWbsModel>
        extends AbstractUserOwnedRepository<M>
        implements IWbsRepository<M> {

    @NotNull
    protected SingularAttribute<AbstractWbsModel, ?> getColumnSort(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return AbstractWbsModel_.created;
        else if (comparator == StatusComparator.INSTANCE) return AbstractWbsModel_.status;
        else if (comparator == DateBeginComparator.INSTANCE) return AbstractWbsModel_.dateBegin;
        else return AbstractWbsModel_.name;
    }

    @NotNull
    protected CriteriaQuery<M> selectModelOrder(@NotNull final Comparator<M> comparator) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<M> criteriaQuery = criteriaBuilder.createQuery(modelClass);
        @NotNull final Root<M> root = criteriaQuery.from(modelClass);
        return criteriaQuery
                .select(root)
                .orderBy(
                        criteriaBuilder.asc(root.get(getColumnSort(comparator)))
                );
    }

    @NotNull
    protected CriteriaQuery<M> selectModelByUserIdOrder(
            @Nullable final String userId,
            @NotNull final Comparator<M> comparator
    ) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<M> criteriaQuery = criteriaBuilder.createQuery(modelClass);
        @NotNull final Root<M> root = criteriaQuery.from(modelClass);
        @NotNull final Join<M, User> user = root.join(AbstractWbsModel_.user);
        return criteriaQuery
                .select(root)
                .where(
                        criteriaBuilder.equal(user.get(User_.id), userId)
                )
                .orderBy(
                        criteriaBuilder.asc(root.get(getColumnSort(comparator)))
                );
    }

    @Nullable
    @Override
    @SneakyThrows
    public M create(@Nullable final User user, @NotNull final String name) {
        @NotNull final M model = newInstance();
        model.setName(name);
        model.setUser(user);
        return add(model);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M create(@Nullable final User user, @NotNull final String name, @NotNull final String description) {
        @NotNull final M model = newInstance();
        model.setName(name);
        model.setUser(user);
        model.setDescription(description);
        return add(model);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M create(
            @Nullable final User user,
            @NotNull final String name,
            @NotNull final String description,
            @Nullable final java.util.Date dateBegin,
            @Nullable final java.util.Date dateEnd
    ) {
        @NotNull final M model = newInstance();
        model.setName(name);
        model.setUser(user);
        model.setDescription(description);
        model.setDateBegin(dateBegin);
        model.setDateEnd(dateEnd);
        return add(model);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        return entityManager
                .createQuery(selectModelOrder(comparator))
                .getResultList();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @NotNull final Comparator<M> comparator) {
        if (userId == null) return Collections.emptyList();
        return entityManager
                .createQuery(selectModelByUserIdOrder(userId, comparator))
                .getResultList();
    }

}

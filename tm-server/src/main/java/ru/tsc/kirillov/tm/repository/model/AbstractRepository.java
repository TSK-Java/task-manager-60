package ru.tsc.kirillov.tm.repository.model;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.kirillov.tm.api.repository.model.IRepository;
import ru.tsc.kirillov.tm.model.AbstractModel;
import ru.tsc.kirillov.tm.model.AbstractModel_;
import ru.tsc.kirillov.tm.util.GenericUtil;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Getter
@Repository
public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {
    
    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @NotNull
    protected final Class<M> modelClass = getModelClass();

    @NotNull
    @SneakyThrows
    protected Class<M> getModelClass() {
        return (Class<M>) GenericUtil.getClassFirst(getClass());
    }

    @Nullable
    protected M getResult(@NotNull final TypedQuery<M> query) {
        @NotNull final List<M> result = query.setMaxResults(1).getResultList();
        if (result.isEmpty()) return null;
        return result.get(0);
    }

    @NotNull
    @SneakyThrows
    protected M newInstance() {
        return modelClass.newInstance();
    }

    @NotNull
    protected CriteriaQuery<M> selectModel() {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<M> criteriaQuery = criteriaBuilder.createQuery(modelClass);
        @NotNull final Root<M> root = criteriaQuery.from(modelClass);
        return criteriaQuery.select(root);
    }

    @NotNull
    protected CriteriaQuery<Long> selectCountModel() {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        @NotNull final Root<M> root = criteriaQuery.from(modelClass);
        return criteriaQuery.select(criteriaBuilder.count(root));
    }

    @NotNull
    protected CriteriaQuery<Long> selectCountModelById(@NotNull final String id) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        @NotNull final Root<M> root = criteriaQuery.from(modelClass);
        return criteriaQuery
                .select(criteriaBuilder.count(root))
                .where(criteriaBuilder.equal(root.get(AbstractModel_.id), id));
    }

    @Nullable
    @Override
    public M add(@NotNull final M model) {
        entityManager.persist(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        @NotNull List<M> result = new ArrayList<>();
        models
                .stream()
                .forEach(model -> result.add(add(model)));
        return result;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        clear();
        return add(models);
    }

    @Override
    public void clear() {
        removeAll(findAll());
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return entityManager
                .createQuery(selectModel())
                .getResultList();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return entityManager
                .createQuery(selectCountModelById(id))
                .getSingleResult() == 1L;
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) {
        return entityManager.find(modelClass, id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final Integer index) {
        @NotNull final TypedQuery<M> query = entityManager
                .createQuery(selectModel())
                .setFirstResult(index);
        return getResult(query);
    }

    @NotNull
    @Override
    public M remove(@NotNull final M model) {
        entityManager.remove(entityManager.getReference(modelClass, model.getId()));
        return model;
    }

    @Override
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null) return;
        collection
                .stream()
                .forEach(this::remove);
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String id) {
        @NotNull final Optional<M> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final Integer index) {
        @NotNull final Optional<M> model = Optional.ofNullable(findOneByIndex(index));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }

    @Nullable
    @Override
    public M update(@NotNull final M model) {
        return entityManager.merge(model);
    }

    @Override
    public long count() {
        return entityManager
                .createQuery(selectCountModel())
                .getSingleResult();
    }

}

package ru.tsc.kirillov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.kirillov.tm.api.repository.model.IUserOwnedRepository;
import ru.tsc.kirillov.tm.model.AbstractUserOwnedModel;
import ru.tsc.kirillov.tm.model.AbstractUserOwnedModel_;
import ru.tsc.kirillov.tm.model.User;
import ru.tsc.kirillov.tm.model.User_;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

@Repository
public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

    @NotNull
    protected CriteriaQuery<M> selectModelByUserId(@Nullable final String userId) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<M> criteriaQuery = criteriaBuilder.createQuery(modelClass);
        @NotNull final Root<M> root = criteriaQuery.from(modelClass);
        @NotNull final Join<M, User> user = root.join(AbstractUserOwnedModel_.user);
        return criteriaQuery
                .select(root)
                .where(
                        criteriaBuilder.equal(user.get(User_.id), userId)
                );
    }

    @NotNull
    protected CriteriaQuery<M> selectModelByUserIdAndId(@Nullable final String userId, @NotNull final String id) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<M> criteriaQuery = criteriaBuilder.createQuery(modelClass);
        @NotNull final Root<M> root = criteriaQuery.from(modelClass);
        @NotNull final Join<M, User> user = root.join(AbstractUserOwnedModel_.user);
        return criteriaQuery
                .select(root)
                .where(
                        criteriaBuilder.equal(user.get(User_.id), userId),
                        criteriaBuilder.equal(root.get(AbstractUserOwnedModel_.id), id)
                );
    }

    @NotNull
    protected CriteriaQuery<Long> selectCountModelByUserId(@Nullable final String userId) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        @NotNull final Root<M> root = criteriaQuery.from(modelClass);
        @NotNull final Join<M, User> user = root.join(AbstractUserOwnedModel_.user);
        return criteriaQuery
                .select(criteriaBuilder.count(root))
                .where(
                        criteriaBuilder.equal(user.get(User_.id), userId)
                );
    }

    @NotNull
    protected CriteriaQuery<Long> selectCountModelByUserIdAndId(
            @Nullable final String userId,
            @NotNull final String id
    ) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        @NotNull final Root<M> root = criteriaQuery.from(modelClass);
        @NotNull final Join<M, User> user = root.join(AbstractUserOwnedModel_.user);
        return criteriaQuery
                .select(criteriaBuilder.count(root))
                .where(
                        criteriaBuilder.equal(user.get(User_.id), userId),
                        criteriaBuilder.equal(root.get(AbstractUserOwnedModel_.id), id)
                );
    }

    @Override
    public void clear(@Nullable final String userId) {
        removeAll(findAll(userId));
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        return entityManager
                .createQuery(selectModelByUserId(userId))
                .getResultList();
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return false;
        return entityManager
                .createQuery(selectCountModelByUserIdAndId(userId, id))
                .getSingleResult() == 1L;
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @NotNull final TypedQuery<M> query = entityManager
                .createQuery(selectModelByUserIdAndId(userId, id));
        return getResult(query);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || index == null) return null;
        @NotNull final TypedQuery<M> query = entityManager
                .createQuery(selectModelByUserId(userId))
                .setFirstResult(index);
        return getResult(query);
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @NotNull final Optional<M> model = Optional.ofNullable(findOneById(userId, id));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || index == null) return null;
        @NotNull final Optional<M> model = Optional.ofNullable(findOneByIndex(userId, index));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }

    @Override
    public long count(@Nullable final String userId) {
        if (userId == null) return 0;
        return entityManager
                .createQuery(selectCountModelByUserId(userId))
                .getSingleResult();
    }

}

package ru.tsc.kirillov.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.kirillov.tm.api.repository.dto.ITaskDtoRepository;
import ru.tsc.kirillov.tm.dto.model.TaskDto;
import ru.tsc.kirillov.tm.dto.model.TaskDto_;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

@Repository
@NoArgsConstructor
public class TaskDtoRepository
        extends AbstractWbsDtoRepository<TaskDto>
        implements ITaskDtoRepository {

    @NotNull
    protected CriteriaQuery<TaskDto> selectTaskProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<TaskDto> criteriaQuery = criteriaBuilder.createQuery(modelClass);
        @NotNull final Root<TaskDto> root = criteriaQuery.from(modelClass);
        return criteriaQuery
                .select(root)
                .where(
                        criteriaBuilder.equal(root.get(TaskDto_.userId), userId),
                        criteriaBuilder.equal(root.get(TaskDto_.projectId), projectId)
                );
    }

    @NotNull
    protected CriteriaDelete<TaskDto> deleteTaskByUserId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaDelete<TaskDto> criteriaDelete = criteriaBuilder.createCriteriaDelete(modelClass);
        @NotNull final Root<TaskDto> root = criteriaDelete.from(modelClass);
        return criteriaDelete
                .where(
                        criteriaBuilder.equal(root.get(TaskDto_.userId), userId),
                        criteriaBuilder.equal(root.get(TaskDto_.projectId), projectId)
                );
    }
    
    @NotNull
    @Override
    public List<TaskDto> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || projectId == null) return Collections.emptyList();
        return entityManager
                .createQuery(selectTaskProjectId(userId, projectId))
                .getResultList();
    }

    @Override
    public void removeAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        entityManager
                .createQuery(deleteTaskByUserId(userId, projectId))
                .executeUpdate();
    }

    @Override
    public void removeAllByProjectList(@Nullable final String userId, @Nullable final String[] projects) {
        Stream.of(projects)
                .filter(p -> p != null && !p.isEmpty())
                .forEach(p -> removeAllByProjectId(userId, p));
    }

}

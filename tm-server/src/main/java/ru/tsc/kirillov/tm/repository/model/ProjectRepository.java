package ru.tsc.kirillov.tm.repository.model;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;
import ru.tsc.kirillov.tm.api.repository.model.IProjectRepository;
import ru.tsc.kirillov.tm.model.Project;

@Repository
@NoArgsConstructor
public class ProjectRepository extends AbstractWbsRepository<Project> implements IProjectRepository {

}

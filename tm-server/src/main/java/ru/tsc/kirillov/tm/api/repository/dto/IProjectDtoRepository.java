package ru.tsc.kirillov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.ProjectDto;

public interface IProjectDtoRepository extends IWbsDtoRepository<ProjectDto> {

    @NotNull
    String[] findAllId(@Nullable String userId);

}
